---
title: Presenteren en Kill your Darlings
date: 2017-09-18
---

Vandaag hebben we ons concept gepresenteerd aan de klassen 1A en 1B. We hadden afgesproken dat Sverre de powerpoint presentatie in het weekend zou gaan maken en deze dan in de drive te zetten. Dit is gelukkig gelukt.
Ons team ging als derde presenteren. Ik deed de introductie en vertelde over ons onderzoek en de conclusies daaruit.

> Daaruit kwam bij mij naar boven dat het toch handig was om toch beter voor te bereiden. Dit omdat ik het deel van ons proces naar concept was vergeten te vertellen. Zoals dat we met ze alle concepten hebben gemaakt en deze naast elkaar hebben neergelegd en uiteindelijk er een hebben gekozen.
> En ook dat het proces van een kaartspel veranderd is omdat we een online aspect vergeten waren erin te zetten.

Sverre vertelde daarna de rest. Over hoe de game precies werkte en hoe dat er in de app uit zal zien. Marleen en Iris schreven de feedback op die er werd gegeven. En Rick filmde ons voor het bewijs dat we hebben gepresenteerd.

Na onze presentatie hadden we vragen zoals, waarom we niet bij een kaartspel zijn gebleven? (dit staat ook in mijn reflectie vlak).

**Creatieve technieken**
Nadat we de presentaties van iedereen hebben gehad zijn we gezamenlijk wezen lunchen. Daarna hebben we een les in creatieve technieken dat was best interessant omdat ze liet zien dat je denkt in verschillende standen. Het begint met de simpele voor de hand liggende en langzamerhand gaat het steeds verder verder en verder met het bedenken wat er nog meer bij dat woord past.

**Blog**
De les creatieve technieken is afgelopen en de workshop blog stond klaar. Verder leren over hoe het precies in elkaar zat. Voor mij was het best lastig omdat ik in het weekend eindelijk door had hoe het werkte, maar de les raakte mij weer even de weg kwijt. Ik denkt dat het komt omdat de concentratie er niet meer was. De volgende keer ga ik even kijken naar een mogelijkheid om wat meer concentratie te hebben tijdens de workshop.
