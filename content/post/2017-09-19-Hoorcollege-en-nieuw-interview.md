---
title: Hoorcollege en nieuw interview
date: 2017-09-19
---

Vandaag is het weer zover de hoorcollege. Deze week gaat het over het onderwerp Prototype. Een onderwerp dat ik heel interessant vind. Tijdens de hoorcollege kreeg je ook te horen hoe precies het proces daarin gaat. Zo moet je niet gelijk wat gaan maken en ervan uitgaan dat het ook echt werkt zonder de testen. Het risico is dan ook heel groot dat je dan iets uitwerkt dat uiteindelijk helemaal niet werkt. Ik vond het ook heel interessant dat hij ook voorbeelden maakten van tv-series en programma's. Door die vergelijkingen begreep ik het ook heel snel.

Na het hoorcollege zijn we met het team wezen overleggen wat we gaan doen. Wat we hebben gehoord dat we ons concept weg moeten gooien. We hebben besloten om bij ons doelgroep te blijven (bouwkunde) en deze keer gaan we interviews afnemen met vragen die wat persoonlijker zijn zodat je meer over de studenten zelf weet (de vorige vragen gingen meer over Rotterdam). Morgen gaan we tijdens onze studio dag naar Academieplein om nieuwe studenten te interviewen en nieuwe informatie winnen.
