---
title: Team opdrachten studiecoaching
date: 2017-09-21
---

Vandaag zijn we met ons team rond om de tafel gegaan. Dit was al van te voren afgesproken. We wilde graag beginnen aan een spelanalyse. Maar we kregen gisteren onze feedback terug van de studiecaching opdracht. Er ontbraken een paar onderdelen en ook waren sommige dingen heel erg onduidelijk.

We hebben toen besloten om deze maar te verbeteren i.p.v. de spelanalyse. Dit omdat sommige graag eerst feedback wilde hebben van de vorige spelanalyse van de eerste literatie. Voor het maken van een nieuw document heeft iris een gezamenlijk document gemaakt zodat we met ze allen erin kunnen werken. Aan de ene kant was het heel handig. Maar op sommige punten ook lasten. Dit omdat soms de pagina verschoof terwijl je aan het typen of lezen was. En ook de manier van iets schrijven van anders. Zo gerbuikte sommige bij een opsomming bolletjes, andere streepjes en andere helemaal geen en zette alles onder elkaar.

Toen het document klaar was vond ik dat het document een stuk beter uitzag dan daarvoor.

**Interview**
Die avond kreeg ik de documenten van Rick en Marleen binnen. De interviews heb ik bij het andere document samengevoegd. Toen deze klaar was heb ik mijn document als word en pdf geëxporteerd zodat iedereen het kan lezen. Daarna heb ik deze twee bestanden op de drive gezet en aangeven in de groep app dat het samengevoegde document online stond.
