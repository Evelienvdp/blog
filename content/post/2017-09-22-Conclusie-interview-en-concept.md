---
title: Conclusie interview en concept
date: 2017-09-22
---

Vandaag heb ik het het samengevoegde document bekeken en een conclusie uit gehaald. Er kwam veel naar voren dat deze studenten graag actief bezig zijn. Iets wat nog niet naar voren kwam bij het vorige interview. Ook de interesse in het gameaspect was anders. Zo hadden we nu wel studenten die graag aan gamen deden, vooral Fifa en Call of Duty. Toch was er wel een overeenkomt te vinden. De studenten vonden het veel fijner om met vrienden te spelen dan alleen. Dit om de competitie met elkaar af te gaan. Maar de gezelligheid en plezier staat op nummer 1.

De conclusie heb ik mijn dummy samengevat en daaruit ben ik gaan kijken naar ideeën voor een concept. Hiervoor ben ik begonnen met een mindmap. Ik schreef woorden uit die uit de conclusie van het interview springen. En daaruit ben ik verder gegaan met woorden die me daar aan denken.
Ineens kreeg ik een idee om studenten buiten wat te laten bouwen met de spullen die ze op straat zien. Zoals een tak, bladeren, oude kranten of blikjes enz. Hier moeten ze een foto van maken om de opdracht te voltooien. Er was ook een idee om de telefoon als een soort wii afstandbediening te gebruiken. Maar voor de rest had ik niet heel erg ver. Ik miste het doel en ook de logica van het hoe spel dan zou moeten werken. Ik zette mijn dummy even op zij om mijn gedachten even op iets anders te zetten. Wie weet krijg ik dit weekend wel inspiratie van iets?
