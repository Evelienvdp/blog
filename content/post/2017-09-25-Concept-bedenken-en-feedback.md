---
title: Concept bedenken en feedback
date: 2017-09-25
---

Vandaag zijn we met groepje om de tafel gegaan en bespraken we onze conclusies van het interview. We waren het er snel mee eens dat het de sportiviteit bij de studenten erg groot was. En dat gezelligheid/teamwork heel belangrijk voor hun was. Maar dan het idee van een concept. Er kwam niet heel veel uit. Ik probeerde mijn vage idee te vertellen maar daar kwamen we ook niet heel ver mee.

**Creatieve technieken toepassen**
We besloten toen om gezamelijk een mindmap. Te maken met ideeën. Dat ging best goed, maar ideeën voor een concept kwamen niet echt naar boven. Toen kwam rick met een nieuw idee om inspiratie te krijgen. We moeste op elk blad drie woorden schrijven waar we aan dachten aan de studie bouwkunde. Als we dat gedaan hadden, moesten we ons blaadje doorgeven en dan op het nieuw blaadje waar al woorden op stonden ook weer nieuwe woorden opschrijven waar je aan die woorden moest denken. Het idee was goed. Maar inspiratie kregen we er niet van. De rest van de dag bleven we brainstormen naar ideeën.

Toen kwam Jantien langs. We vertelde haar dat we niet echt op een idee kwamen, met de methodes die we hadden gebruikt. Zij kwam toen met een doosje met allerlei verschillende methodes waarmee je ideeën zou kunnen krijgen. We bekeken de kaartjes erin en ik moet toegeven dat er inderdaad wel een paar goede tussen zaten. Goede methodes die we kunnen gebruiken voor nieuwe concept ideeën.
