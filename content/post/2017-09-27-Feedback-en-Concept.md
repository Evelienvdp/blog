---
title: Feedback en Concept
date: 2017-09-27
---

*Vandaag hadden we de eerste twee uur studiecoaching. Tijdens deze les moesten we elkaar feedback geven over de samenwerking. Ik kreeg feedback van Marleen, Rick en Sverre. Na het lezen van de feedback was ik best wel tevreden en ook was ik het redelijk eens met de feedback waar ik aan moet werken.*

Van Marleen kreeg ik de feedback dat ik goed nadenk en beargumenteer wat en waarom ik iets wil. Ook kreeg ik als feedback te horen dat ik me soms op de achtergrond houd en soms iets te vaak uit mijn eigen perspectief kijk. Met deze feedback was ik het best eens en ik ga er ook echt wat aan doen.
Van Rick kreeg ik als feedback dat ik het teamlid ben met de ideeën. Ook kreeg ik te horen dat ik wat moet doen met de feedback die ze geven op mijn ideeën. Zelf snapte ik er niet echt veel van wat hij daarmee bedoelde en als ik dan naar de voorbeeldsituatie kijk zie ik alleen iteratie 1 en 2 staan. Ik heb daarom ook aan hem gevraagd wat hij er precies mee bedoelde en een voorbeeld van zou kunnen geven. Hij gaf daar een antwoord op en ik neem die ook mee.
Sverre had dezelfde feedback gegeven als Marleen en Rick.

Verder op de dag zijn we bezig gegaan met het concept. We miste namelijk nog dat ene stukje in het spel dat het geheel compleet zou maken. Na veel gebrainstormd kwamen we er nog steeds niet uit. We hebben toen om Jantiens hulp gevraagd. Uiteindelijk in een ingeving kwam een idee om een puzzelstukje in de doos te stoppen waarmee de teams samen moeten werken.
