---
title: Werkcollege met lego blokjes
date: 2017-09-28
---

Vandaag hebben we een werkcollege gehad van het hoorcollege van afgelopen dinsdag. Ik vond het een leuke en interessante les. We moesten in teamverband lego blokjes orderenen onder verschillende onderwerpen. Wat mij opviel was dat je ze op allerlei verschillende soorten manieren kon onderscheiden. Later moesten we dit ook gaan doen met afbeeldingen.

Aan het einde van het werkcollege zijn we met het team aan een tafel gaan zitten om aan het prototype te gaan werken.
De taken zijn als volg verdeeld:
 - Marleen doet de doos
 - Sverre doet sloten
 - Rick doet een slot
 - Iris en Evelien doen de schermen voor de app

Samen met Iris hebben we overlegd wat we precies wilde laten zien en hebben dat in ons dummy uitgeschreven zodat we een overzicht hadden. Later hebben we gekozen wie welk scherm ging maken en als volgt zijn we begonnen met het maken van de schermen op papier.
