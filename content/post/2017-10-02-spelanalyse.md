---
title: Spelanalyse
date: 2017-10-02
---

*Vandaag hebben we spelanalyses gemaakt van ons spel. Voordat we begonnen met de analyse hebben we alles klaargezet en heb ik de spelregels even uitgeschreven voor een goed beeld van het spel.*

Het eerste groepje vond het spel duidelijk wel hadden ze aanmerkingen omdat we het menuscherm en scherm voor code-overzicht miste. Ook kwamen ze nog met een vraag of er verschillende routes zijn die de studenten lopen of dat ze dezelfde route lopen. We hebben daarop verteld dat er verschillende routes zijn. Tijdens de spelanalyse heb ik alle punten opgeschreven die ik tegen kwam tijdens het spel. Bij hun was duidelijk wel een glimlach te zien. Nadat het groepje klaar was hebben we besproken wat we hebben ontdekt en wat we nog kunnen verbeteren. We hebben nieuwe schermen gemaakt en deze toegevoegd aan de rest van het prototype.

Bij het tweede groepje ging het toch wel anders. Dit omdat het alweer 16:00 was en deze studenten heel graag naar huis wilde. Ze lazen de spelregels heel snel door en daar kwam duidelijk naar boven dat de spelregels te lang was. Ze hadden het namelijk te snel gelezen en daardoor wisten ze soms niet precies wat ze moesten doen tijdens het spelen van het spel. Ook kwam weer naar boven dat het code overzicht onduidelijk was. Wel vonden ze het idee van het spel wel leuk.

Als laatste hebben we gevraagd of Jantien het spel wilde uitspelen. Daar kwamen een heleboel punten uit waar we nog aan zouden kunnen werken. Zo kwam de vraag wat is precies het nut van die QR-code waarom kunnen vragen niet naar boven poppen als ze op de locatie zelf zijn. Is er iets van en tijdlimiet. En ook het einddoel miste. Hiermee gaan we nog mee verder.
