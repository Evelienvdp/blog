---
title: Iteratie documenten klaarmaken
date: 2017-10-03
---

Deze dinsdag zijn we alle documenten voor iteratie 1 bij elkaar aan het zetten. Ik moest de spelregels aanpassen naar veel minder tekst. En ook de spelanalyse uitschrijven. De spelanalyse vond ik op sommige momenten best lastig te formulieren en heb om Iris hulp gevraagd of zij het even door wilde lezen, of ik nog iets miste. Toen het gedaan was heb ik de bestanden op de drive gezet. Zodat Rick ze in het gezamenlijk bestand kan zetten.

**Sketch**
Ook had ik weer les in Sketch. En deze opdracht was wel even lastig. We moesten met symbolen werken en dat vond ik heel verwarrend. Omdat je ook gewoon de vormen kan kopiëren en plakken. Na een uitleg snap ik het een stuk beter en heb ik de class en thuisopdracht gemaakt.
