---
title: Presenteren
date: 2017-10-04
---

Vandaag was het dan zover ons spel presenteren aan projectdocent, vakdocent en alumni. We hadden afgelopen week afgesproken dat Marleen de presentatie zou maken. Nadat ze dat gedaan had, hadden we een taakverdeling gedaan wie wat ging doen. Rick ging filmen. Iris ging aantekeningen maken. Sverre ging het begin en de doelgroep presenteren. Marleen vertelde over het concept. En ik over het prototype en de uitslag van de spelanalyses. Het begin ging goed. Maar toen ik moest gaan presenteren had ik ineens een black-out. Ik kan me ook niet meer zoveel ervan herinneren. Wat ik wel weet is dat ik best wat dingen ben vergeten te vertellen.

> Volgende keer voor een presentatie beter voorbereiden

Uit de feedback van de docenten en alumni kwam veel positiviteit naar boven. Ook kregen we tips zodat we goed verder kunnen met het ontwikkelen van het spel.

Voor de rest van de dag heb ik naar presentaties gekeken en ik vond het best wel leuk. Vooral om de ideeën van andere te zien.
