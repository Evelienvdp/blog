---
title: Prototype werkcollege
date: 2017-10-05
---

Vandaag hadden we in het begin van de dag een 0-meting Engels en Nederlands. Dit was voor mij op sommige momenten makkelijk en andere momenten moeilijk. Ben benieuwd wat de uitslag zal zijn.

Ook hadden we het werkcollege dat we nog moesten krijgen. Dit keer ging het over het onderwerp Prototype. Tijdens deze les hebben we het hele proces van prototypen behandeld een leuk proces. Zo moesten we eerst kiezen voor welk onderdeel we wilde gaan, we kozen voor de deur. En voor de deur moesten we een idee creëer dat de studenten met elkaar zou verbinden. We hebben verschillende ideeën opgeschreven en daaruit hebben we er twee uitgekozen: Een hand als deurklink. En een deur waar je op mag tekenen. Toen we daar een prototype van hadden gemaakt moesten andere het uitproberen zodat wij het konden analyseren.
Het was een leuk en interessant proces dat je goed kan meenemen naar het project van Design Challenge.
