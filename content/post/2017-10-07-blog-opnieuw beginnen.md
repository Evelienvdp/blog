﻿---
title: Blog opnieuw beginnen
date: 2017-10-07
---

*Vandaag ben ik opnieuw begonnen met het maken van mijn blog via gitlab.com.*

Ik kwam er gewoon niet uit met hoe het allemaal precies werkte. Dit kwam doordat ik zoveel had gedaan waardoor ik het overzicht niet meer kon vinden. Daarom heb ik besloten om op deze zaterdag helemaal vanaf het begin te beginnen. Ik deed alles wat er op de uitleg stond online voor het maken van een blog. Al gauw werd het me een stuk duidelijker. Dit omdat ik het deed op een rustige plek zodat ik alles goed kon volgen. Uiteindelijk heb ik Smartgit en Atom op mijn laptop gezet. Alles had ik gedaan zoals er stond op de website. Toch kwam ik er niet helemaal uit hoe ik precies nieuwe post kan plaatsen. Daarom ga ik a.s. maandag aan mijn klasgenoten vragen of zij mij daar mee kunnen helpen.

> Door de blog opnieuw te maken heb ik een beter beeld gekregen van hoe het eigenlijk in zijn werk gaat (op het plaatsen van een blog na). Zelf vond ik dat dit een goede manier was, want nu begrijp ik wat ik heb gedaan.
