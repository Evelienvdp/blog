﻿---
title: Route plannen voor spel
date: 2017-10-09
---

Vandaag heb ik met mijn team rond om de tafel gezeten om de feedback van de presentatie te gebruiken om onze game nog beter te verbeteren. We hebben met sticky notes op het raam (achter onze tafel) worden geschreven met onderwerpen die nu heel belangrijk zijn. Zo hebben we onderwerpen als: Spelonderdelen, de route van de "speurtocht", einddoelen en prototype. We hebben met zijn alle al onze ideeën op een sticky note geschreven en deze bij een onderwerp geplaatst. Dit heeft een goed beeld gegeven. Zo zijn we erachter gekomen dat het verstandig is om een keer een route te lopen, zodat we een beeld kunnen krijgen hoe lang die route daadwerkelijk is. Ook zijn we wezen speculeren over het feit om een derde opdracht in onze game te plaatsen, maar na het overleg over hoe lang de route precies zal zijn. En hoe lang het zou duren als de spelers de maquette bijenkaar hebben geplaatst. Is er toch naar voren gekomen om niet een derde opdracht erbij te doen, omdat het dan heel lang wordt.

**De routes**
We hebben met elkaar overlegt over welke routes we precies willen gaan lopen. Daaruit zijn twee routes gekomen. Een route die door het centrum gaat, en een andere route die langs de grens van Rotterdam zuid gaat. In overleg is er besloten om de routes morgen te lopen. Dit omdat we toch vrij zijn en daardoor ruim de tijd hebben om de route te lopen. We hebben besloten in groepjes te gaan lopen. Marleen en ik gaan de route van de stad lopen. Sverre en Rick de route langs Rotterdam zuid. De routes hebben we gezamenlijk uitgestippeld en uitgeprint zodat we precies weten hoe we moeten lopen.

**Blog**
Verder heb ik vandaag aan mijn teamleden gevraagd of ze mij wilde uitleggen hoe het precies werkt het posten van een blog. Dit wilde ze wel doen en eindelijk begrijp ik nu hoe het precies allemaal werkt. Als ik er nu naar kijk is het best makkelijk om te doen, en eigenlijk ook wel leuk. Nu is er alleen een dingetje waar ik nog mee zit en dat is het plaatsen van foto's in mijn blogs. Het lukt me nog steeds niet. En ik begrijp ook niet wat ik verkeerd doe. Ik had gehoord dat ik goed moest kijken naar de naam van mijn afbeelding, is het JPG, JPEG, jpg, jpeg? In de info over mijn afbeelding staag JPEG-afbeelding. Maar welke manier ik ook probeer ik krijg het maar niet voor elkaar. Omdat ik en mijn klasgenoten er ook niet achter kwamen heb ik het aan een docent gevraagd. Maar ook hij kwam er niet uit.

> Ik ga me er nog verder in verdiepen om dit toch voor elkaar te krijgen. Ik wil namelijk dat het uiteindelijk wel gaat lukken. Ik ga dit doen door de foto's in een andere bestand te plaatsten of nieuwe foto's maken.
