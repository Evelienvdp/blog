﻿---
title: Route lopen
date: 2017-10-10
---

Vandaag was het zover het lopen van de route door Rotterdam. Ik wist niet wat ik van de route kon verwachten. Dit omdat ik niet heel erg bekend ben in Rotterdam. Maar het leek me wel leuk om nieuwe dingen van deze stad te zien. In de trein naar Rotterdam kreeg ik te horen dat Marleen ziek was en dus niet mee kon lopen met de route. Ik had toen samen met Rick en Sverre besloten om samen de route van hun te lopen. Dit omdat de route van de stad nog niet heel erg duidelijk was. 

Op het verzamel punt (Hogeschool Academieplein) zijn we begonnen met de route. Tijdens deze route maken we foto's van gebouwen die iets weg hebben van architectuur. Zo kwamen we snel restaurant de Machinist tegen en de Euromast. De route duurde uiteindelijk toch iets langer dan verwacht, namelijk 2,5 uur. De maximale tijd die we wilde geven was 2 uur en dus hadden we een conclusie getrokken dat we morgen de route in gaan korten. Want dit is gewoon te lang als de doelgroep daarna ook nog een maquette moeten maken.

> Door de route te lopen had ik wel een goed beeld gekregen over hoe lang het precies zal duren, en of het niet te lang word (uiteindelijk dus wel). Ook voor mezelf vond ik het wel leuk om de route te lopen, dit omdat ik in dit gedeelte van Rotterdam nog nooit ben geweest.