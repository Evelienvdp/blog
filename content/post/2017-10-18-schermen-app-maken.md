﻿---
title: Schermen app maken
date: 2017-10-18
---

Vandaag heb ik de schermen voor de app gemaakt. Deze schermen heb ik digitaal in het programma Sketch gemaakt. Dit kwartaal heb ik de tools voor design gevolgd voor het programma Sketch en ik vind dat ik daar eens gebruik moet maken. Tijdens het maken van de schermen kwam ik erachter dat in Sketch werken een stuk makkelijker gaat, als het gaat om schermen voor apps en websites, in vergelijking met adobe Illustrator. Tijdens het maken van de schermen heb ik gekeken naar de paper prototypes van de vorige iteratie zodat ik duidelijk kan zien wat er wel en niet in moet. En ook om te kijken over er op sommige plekken wat verbetering is. Zo heb ik voor het menu besloten dat deze schuifend naar het scherm komt, i.p.v. op een knop te drukken. Verder heb ik architecture structuur op de achtergrond geplaatst. Dit om een bouwkundig beeld te creëer. 
Nadat ik mijn ontwerp klaar had heb ik deze op de drive gezet en tegen mijn teamleden via de app verteld dat ze op de drive staan en of ze wilde checken of ik nog iets miste. Dit geeft mij namelijk een goed beeld en ook kan ik mezelf verbeteren. Ik kreeg in de avond een berichtje waarin ze vertelde dat het er goed uitzag en dat ik alleen de knoppen bij de goed en fout scherm vergeten was erbij te zetten. Deze heb ik ook gelijk aangepast.
