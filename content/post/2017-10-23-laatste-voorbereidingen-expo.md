---
title: Laatste voorbereidingen expo
date: 2017-10-23
---

Vandaag hebben we met elkaar de laatste voorbereidingen gemaakt voor de expo die a.s woensdag plaats zal vinden. Ik heb deze dag de telefoon dummy opnieuw gemaakt. Dit omdat ik niet echt tevreden was, en ook omdat er op school ander materiaal was die ik beter vond om te gebruiken dan hetgeen dat ik al had (namelijk heel dik karton). Uit het dunne karton dat op school was heb ik vijf telefoon cases gemaakt. Verder heb ik ook de schermen van de app uitgeprint op school. Hiervoor moest ik wel eerst programma's erop zetten omdat het anders niet kan. Uiteindelijk na het installeren is het gelukt en heb ik ook de kaart van Rotterdam, de moodboards en Visualisatie van concept uitgeprint.
