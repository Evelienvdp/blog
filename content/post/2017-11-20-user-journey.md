---
title: User Yourney
date: 2017-11-20
---

Vandaag ben ik met het team verder gegaan met de opdrachten van volgende week. We lopen lekker voor op schema en voor mij geeft dat een rustig gevoel. In het begin van de ochtend hebben we de planning er weer bijgehaald en zijn we gaan kijken wat er moet gebeuren.
Zo moet er een ontwerp criteria, testplan van concepten, 3 low-fit prototypes en de user journey. We zijn gezamenlijk wezen kijken wat het allemaal precies inhield en daar kwamen we eracher dat de user journey heel veel werk was en ook dat we het beste daarmee moesten beginnen, omdat we met die informatie verder konden werken.

**User journey**
Zo gezegd zo gedaan. Omdat er veel onderzoek in de user journey voor moest komen hebben we de onderzoeken onder elkaar verdeeld. Zo heb ik de context en stakeholde gemaakt. In het begin vond ik het best lastig en begreep ik er niet zo heel veel van. Sebastiaan heeft toen erbij geholpen. En uiteindelijk begrijp ik nu wat er in moest komen te staan. Ik heb de doelgroep verdeeld in vier onderdelen waaronder online interactie, sociale interactie, vervoer en betaalde diensten. Uit die vier hoofd onderwerpen heb ik alle verschillende onderdelen die daar bij horen toegevoegd. Uit eindelijk heb ik ze in de computer wat netter uitgewerkt zo dat het wat makkelijker en duidelijker zichtbaar is. In de afbeelding zie je waar de doelgroep mee te maken krijgt als hij daadwerkelijk in paard is. En welke mogelijkheden er allemaal zijn.

> Ik vond het in het begin heel lastig, en ik begreep ook niet helemaal wat doel van was. Maar nadat de onderzoeken naar persona en outcomes ook gedaan waren kreeg ik langzamerhand een beter beeld.

Aan het einde van de dag zijn we ook bezig geweest om de user journey in tabellen te plaatsen voor een duidelijk overzicht. Hierin stelde we de hele reis van de doelgroep vast. Van het zien van de reclame tot het bezoeken en verlaten van paard.

> Voor mij was dit de meest duidelijke en nuttige onderzoek dat we gedaan hebben. Dit omdat je alle vragen die je tegenkomt kan onderzoeken. En uit die vragen misschien wel antwoorden vinden.
