---
title: Feedback en problemen vastleggen
date: 2017-11-22
---

Vandaag gingen we verder met het maken van de user journey. Deze moest namelijk nog afgemaakt worden. Dit is uiteindelijk ook gelukt.

**Feedback**
Verder op de dag konden we om feedback vragen aan Raoul. Daar kwamen we erachter dat we bij de user journey de kansen waren vergeten en ook dat het er wat duidelijker uit moet zien. Hij vertelde ook dat we verder moesten kijken. Zo kwam hij op ideeën om de omgeving in Paard te veranderen. Maar ook om een probleem vast te stellen waarom de 38+ niet iedere daar heen zou gaan (zoals bijvoorbeeld de kinderen of werk). Zo begon hij te vertellen over een kinderopvang naast Paard. Zelf had ik een gevoel van hoe komt hij ineens op die ideeën.

Na de feedback zijn we verder gegaan met het maken van het ontwerpcritera. De problemen vast stellen die we in onze user journey tegen zijn gekomen. Zo kregen we al snel een lijstje met: het zoeken naar de locatie. Kosten van de drankjes. In welke zaal zit je enz. Zelf zat ik een beetje in dubio want we hadden alleen maar een blaadje met kleine info over de doelgroep. En ik vond dat die informatie veel te weinig is. Er moet nog meer onderzoek komen. Dus ben ik met mijn teamleden in gesprek gegaan en heb ik verteld dat we nog meer informatie nodig hebben. Ze waren het met mij eens en we hebben toen besloten om interviews te houden. Geen enquetes. Waarom niet?
1. De kans op antwoord is heel klein.
2. De antwoorden die je krijgt zijn kort en krachtig
3. Je kan niet op de antwoorden doorvragen

Dus hebben we met het hele team vragen gemaakt die we dan aan de doelgroep kunnen stellen. We hebben dan ook afgesproken dat we a.s. maandag allemaal interviews hebben afgelegd zodat we met die informatie verder kunnen kijken.  
