---
title: Conclussie interviews en bedenken concepten
date: 2017-11-27
---

Het is weer maandag een nieuwe week en na het geven van de inteviews denkt ik wijzer te zijn geworden. Wat begon als een vervelende ochtend met vertraging werd al gauw een drukke dag. Om te beginnen ben ik eerst al mijn interviews wezen uitschrijven. Na het besluisteren van de gesprekken raakte ik zelf diep van binnen best enthousiast. Dit omdat ik vond dat ik de interviews goed had afgenomen. Op elk antwoord dat doorgevraagd kon worden heb ik doorgevraagd. En ik was rustig en goed voorbereid.

Nadat ik het had uitgeschreven heb ik ook gekeken naar de interviews van Phillip en Bram. Daaruit kwam grotendeels hetzelfde. Op de muzieksmaak na dan. Maar dat is logisch. Niet iedereen houd van dezelfde muziek. Ik heb daaruit een conclusie getrokken

*De meerderheid van de doelgroep zit grotendeels achter zijn/haar telefoon. Op de telefoon worden de "standaard" apps gebruikt. Zoals nieuws, buitenradar, Facebook, whats app enz. Wat de doelgroep het allerbelangrijkste vind aan een app is: dat deze simpel, to the point en je kan de app gelijkgebruiken zonder nog enig dingen up to daten. Verder kwam ik tot de conclusie dat mensen die uit de regio van Den Haag wonen (of zijn opgegroeid) het Paard kennen. Maar een groot deel van de mensen die buiten de regio wonen (of zijn opgegroeid) kennen het Paard helemaal niet.*

*Wat de doelgroep ook heel belangrijk vind op een feest of evenement is een de gezelligheid en de interactie met de artiest en/of de andere bezoekers/feestgangers. Ook wekt bij de doelgroep de muziek uit de jaren 70/80/90 nostalgie op (waarbij ik bij mij interview blije opgewekte gezichten zag toen ik het daar over had)*

Ik heb de conclusie in een bestand gezet en in dropbox geplaatst. Toen heb ik met de teamleden de conclusie besproken. Na het overleg zijn we met de planning geweest om te kijken wat er nog precies moet gebeuren. Zo was het nu tijd om concepten te bedenken, low fit prototype en een testplan te maken. Allereerst gingen we gezamenlijk concepten bedenken. Dat ging wel goed, alleen een teamlid vond het idee dat Raoul had verteld tijdens de feedback HET beste concept. Ik daarentegen had zo mijn twijfels. Ik kon namelijk nergens uit de onderzoeken uit halen dat de mensen er niet heen gingen vanwege de kinderen. En daarbij of ze dan hun kinderen daar wel naartoe willen brengen. Toch hebben we het concept uitgeschreven.
Daarna zijn Femke en ik verder gegaan met het bedenken van de concepten. Hieruit kwamen we op twee ideeën.
1. Het Paard organiseert thema avonden in de stijlen van de jaren 70/80/90
2. Je kan punten sparen bij Paard om kortingen te krijgen bij je volgende bezoek en/of tijdens je eerste bezoek.

Zelf vond ik ze niet heel erg sterk, maar ik kon nergens op een ander idee komen. Dus hebben we deze uitgeschreven. Er kwamen ook nog andere ideeën over een wallet die je in de app zou kunnen hebben. Zo kan je een bepaald saldo erop zetten en daarmee je drankjes betalen. En als je geld dan op is kan je geen drankjes meer kopen. Dit is voor mensen die bang zijn om te veel te betalen.
