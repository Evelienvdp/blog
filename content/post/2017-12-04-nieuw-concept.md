---
title: Nieuw concepte
date: 2017-12-04
---

*Vandaag was het een rustige ochtend. Alles ging zoals het zou moeten gaan en we begonnen de ochtend met het maken van een planning. Zo besloten we om de gemaakte prototypes te testen bij de doelgroep. Ik heb toen onze praktijkdocent Jantien erbij gehaald met de vraag of zij het wilde testen.*

Al snel kregen we feedback over onze concepten en zelf vond ik dat wel prettig. Zo kwam er in de concepten duidelijk naar boven dat de doelgroep erg gierig waren. Er werd dan ook aan ons gevraagd of er uit de onderzoeken naar boven kwam dat ze inderdaad gierig waren. En dat was niet zo. Bij het ander concept hadden we een day care en daarop kregen we de vraag of de doelgroep wel hun kind wil afgeven aan een daycare en dan s'avonds weer naar huis te gaan met het kind. En hier kwam ook weer het woord nee uit. Want we wisten dat namelijk niet. En uit een paar interviews kwam toch wel naar boven dat ze niet hun kind wilde afgeven. Ook kregen we te horen dat dit idee juist voor mee problemen zou gaan zorgen. We kregen toen de feedback om ons meer te focussen op het terugkoppelen, zodat de ideeën die we maken ook aan sluiten op het onderzoek.

**De ideeën**
Hiermee zijn we verder gegaan. En hebben we een oud idee "thema avonden" opgepakt. De hele dag zijn we bezig geweest met het bedenken van de sfeer die we willen creëer op de avond en ook wat voor interactief afspect we erbij kunnen toevoegen. Dit hebben we gedaan door allemaal ideeën op te schrijven die we maar konden verzinnen op zo een avond. Zo kwamen we op karaoke, dans wedstrijden, verkleed in de stijl van het thema, kleuren naar het thema, muziek passend bij het thema enz. Ik heb toen op een blaadje de allerbelangrijkste kern ideeën opgeschreven die een belangrijk deel gaan nemen voor de sfeer die zal worden toegepast tijdens die avond.

Het waren allemaal leuke ideeën die een bepaalde sfeer creëer in Paard, maar de vraag werd wel hoe gaan we er een interactief element in voor komen. Ik ben toen online wezen kijken naar verschillende ideeën maar ik kon niet echt op een idee komen. Tot dat Sebastiaan het over een interatieve vloeren en muren kwam. Hij liet ons de beelden zien en dit was zeker een goed interactief element om toe te passen in dit concept.
Jantien kwam weer langs om te kijken hoe het nu ging en we vertelde ons idee. We kregen toen als feedback dat we basis nu hebben van het concept, maar dat we nu verder moeten kijken. Hiermee werden de touch point bedoelt. Want op welke manier kan de doelgroep geprikkeld worden om naar dat event te gaan? Hier zijn we nog mee bezig en morgen gaan we er mee verder.
