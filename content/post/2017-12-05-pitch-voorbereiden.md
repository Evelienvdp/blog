---
title: Pitch voorbereiden
date: 2017-12-05
---

Vandaag ben ik met het team na het hoorcollege om de tafel gaan zitten. We moesten ons nog gaan voorbereiden voor de pitch die morgen zal plaats vinden. Nadat we gisteren een nieuw concept hebben gemaakt, moest er nog verder gekeken worden naar welke sfeer we precies wilde laten zien als touch points.
Hier zijn we even mee bezig geweest.

**De Pitch**
Daarna zijn we wezen kijken wat we precies moesten gaan doen voor de pitch. Zo moest er een pitch geschreven worden en eventueel beeldmateriaal voor de ondersteuning. Deze taken hebben we verdeeld en afgesproken dat dit voor morgen af zou zijn. Bram zo de pitch schrijven. Philip zal de poster en ander beeldmateriaal maken/opzoeken. En ik zal een storyboard maken. Nadat we dit hebben overlegt ben ik samen met Bram op school gebleven om aan de Pitch te werken. Het ging voor mijn gevoel goed. Ik heb het verhaal dat vertelt ging worden in de Pitch ook duidelijk en simpel met een storyboard verwoord. Ook heb ik samen met Bram zijn Pitch doorgenomen om te kijken of het allemaal precies een minuut was. En dat was ook zo.
