---
title: De Pitch en recap
date: 2017-12-06
---﻿

Het was dan nu zover. De pitch presenteren aan de klant van Paard. Om 9.00 uur was iedereen verzameld van ons team. We werden naar een tafel gewezen en hebben onze pitch spullen op de tafel gezet om te presenteren. Philip is samen met Sebastiaan en Bram nog naar beneden gegaan om de laatste dingetjes te printen. Ondertussen waren Femke en ik aan het wachten.
Al snel kwamen de drie naar boven met allerlei soorten presentatie bladen. En zelf vond ik ze er erg goed uitzien.

**De Pitch**
9.30 eindelijk begonnen te pitches. Maar ja, wij waren nummer 8 en ze begonnen eerst bij de oneven getallen. Dus rustig blijven wachten. En toen was het eindelijk zover. Ons team mocht pitchen. Bram deed de pitch en hij vertelde dat op een leuke en enthousiaste toon. Nadat Bram zijn verhaal had verteld kwam de feedback van de klant. Hij leek niet echt enthousiast met wat we kwamen. Dit kwam waarschijnlijk omdat we ons meer hadden gefocust op de sfeer dan op het interactief element. Hij vertelde ook dat we maar onderzoek moesten doen en kijken of er niet andere bedrijven zijn die ook thema avonden doen en/of ook een interactieve vloer hebben. Ook vertelde hij dat het idee van ons meer om het organiseren ging en dat we moesten nagaan naar wat Paard vroeg. Mensen stimuleren om terug te laten komen naar Paard.

Na de pitches hebben we met ons team gekeken van wat er nog moet gebeuren. Zo moest de erecap en de rapportage van de concepttest worden ingeleverd vandaag. We hebben gelijk de taken verdeeld. Bram, Sebastiaan en Philip gingen de teksten schrijven voor de recap en de rapportage van concepttest schrijven. Femke en ik gingen de opmaak maken voor de recap en de tekst daarvan erin toevoegen.

**User Journey**
We zagen dat de User journey ook in de recap moest en dus heb ik de taak op me genemen om de gemaakte user journey wat simpeler en duidelijker maken van wat we al hadden. Terwijl Femke de opmaak ging maken. Als voorbeeld heb ik de user journey van Paard gebruikt zodat ik ongeveer wist hoe ik het moest vormgeven.

Toen ik daar klaar mee was heb ik de achterkant van de recap gemaakt om Femke verder te helpen. Ik had het document van Femke via airdrop gekregen zodat ik er aan kon werken. Ik heb de teksten in het document gezet en de vormgeving van Femke aangehouden. Wel heb ik hier en daar een paar aanpassingen gedaan zodat het beeld er wat beter uitziet. Toen ik hiermee klaar was heb ik het document naar Femke geairdropt zodat zij de twee documenten kon samenvoegen en inleveren.
