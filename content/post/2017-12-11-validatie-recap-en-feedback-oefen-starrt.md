﻿---
title: Validatie recap en feedback oefen-STARRT
date: 2017-12-11
---

Vandaag was het een hele hevige sneeuwige dag. Het was voor mij ook een twijfel geval of ik wel naar school zou gaan. Dit omdat ik niet echt zeker wist dat ik aan het einde van de dag weer veilig thuis zal komen. 
Uiteindelijk heb ik toch mijn stoute schoenen aangetrokken en heb ik besloten om toch met het openbaar vervoer naar school te gaan. 
Toen ik op school aankwam werdt er vertelt wat er allemaal moest gaan gebeuren deze week. Daar kwam naar voren dat we verder gaan kijken met het uitwerken van het concept en met de STARRT bezig zullen zijn.

Verder werd vandaag de validatie van de recap gedaan en feedback van de oefen-STARRT. Ondertussen werd het weer in de gaten gehouden om te kijken of het nog veilig is om op school te blijven.
De rest van mijn team was niet aanwezig en zijn thuis gebeleven vanwege de sneeuw. Wel hebben we met elkaar gecommuniceerd via de whattsapp en ik ben tijdens het wachten op de validatie wezen onderzoeken naar de verschillende mogelijkheden die er zijn met een interactive vloer. 

Validatie recap
Toen was het tijd voor de validatie van de recap. Er werd feedback gegeven over het feit dat er weinig simpelheid in zat. Ook ontbrak er in de recap de conclusie van onze onderzoeken. Ik ga dan ook deze feedback meenemen en de recap aanpassen. Simpeler en met de conclusie erbij.

Feedback oefen-STARRT
Ook kreeg ik de feedback van mijn oefen-STARRT. Ik kreeg te horen dat ik goed moest nadenken over wat ik precies wilde bewijzen. Dit omdat er drie competenties in werden verteld. Ik ga nu ook als ik een STARRT ga schrijven opletten wat bij welke competentie hoort zodat het straks voor iedereen duidelijk is.

Ondertussen het krijgen van de feedback voor de oefen-STARRT werd er ook vertelt dat de school om 13.00 uur zou gaan sluiten. Dus na het verkregen van de feedback heb ik de school verlaten en ben ik naar huis gegaan (dat duurde helaas alleen wat langer door het treinverkeer).   :(