﻿---
title: Workshop Letterontwerp
date: 2017-12-12
---

Vandaag heb ik een workshop over letterontwerp gehad. Dit was een workshop waarbij je meer te weten kwam over het ontwerp van lettertypes. Helaas door het sneeuwige weer heb ik een half uur van de workshop gemist, omdat ik moest omrijden met de trein. Uiteindelijk toen ik er was kreeg ik een blad en heb ik meegedaan met de workshop. Ik vond het persoonlijk heel erg leerzaam en ook heel erg leuk. We hebben met de hele groep gezamenlijk opdrachtjes gedaan op papier. Zo moetsten we woorden opschrijven en daarbij iets toepassen. Een hoge lijn trekken of een lage lijn trekken. Dit was interessant omdat je heel ander beeld kreeg en dat was alleen maar omdat je de lijn hoogte had aangepast. 
Daarna ben ik bezig geweest met kalligraferen. En met mijn hele dikke stift ging dat heel erg slecht... Maar naar een hele tijd oefenen en letters na schrijven begon het er toch een heel stuk beter uit te zien.

> Ik vond deze workshop heel erg nuttig, vooral omdat je de achterliggende gedachten moet gebruiken om een bepaald lettertypen te gebruiken. Ipv dit vind ik een mooi lettertype dus gebruik ik die ook maar.

Volgende week ga ik er weer heen naar deel 2 van de workshop Letterontwerp.