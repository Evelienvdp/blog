﻿---
title: Concept Bijwerken
date: 2017-12-18
---

Vandaag zijn we met het hele team rond om de tafel gaan zitten. We hebben overlegt over het geen dat moest gebeuren. Zo moesten er delivebles voor deze week worden gemaakt zoals: User journey toekomstige situatie, styleguide en design rationale. Tijdens dit overleg vond ik dat ik wat moest zeggen over het concept. Namelijk dat ik nog best mijn twijfels hebt over het concept omdat deze niet goed onderbouwd wordt door onderzoeken die door ons zijn gepleegd. Er werd grotendeels meer gekeken naar het maken van de deliverbles dan naar het onderzoek. Grotendeels van mijn teamleden waren het met mij eens. En dus hebben we afgesproken met elkaar om eerst aan het concept te sleutelen voor een betere oplossing. Zo werd er nagedacht over het idee om iets van een prototype te maken waarmee we de gebruikers kunnen stimuleren. Zodat ze een sfeer krijgen te zien en het gevoel krijgen dat ze ook daadwerkelijk in Paard zijn. Het is namelijk best lastig om dit concept te testen bij de doelgroep. Grotendeels omdat het concept om de sfeer en gevoel gaat. En niet zo maar een app. Dus kwam ik met een idee om een soort virtual reality versie te maken. Ik ben toen online wezen kijken naar opties om dit test idee te realiseren. Daar kwam ik uit op een website die een gratis demo model aanbied waarmee je een 360 graden beeld te creëer. Ik ben hiermee wezen kijken.

**Feedback**
Mio kwam ook langs om te kijken hoe het met ons ging en eventueel feedback geven. Zo vertelde wij dat we vast zitten met het concept en dat het plaatje niet helemaal compleet is.
Hij gaf ons de tip om te kijken naar de eventuele functies van een interactieve vloer en welke daadwerkelijk goed van toepassing kunnen zijn voor Paard. Daarna kijken welke het beste van goede toepassing zal zijn (dirigeren - convergeren). 

Zo gezegd zo gedaan en hiermee zijn wij als team verder wezen kijken. We hebben toen ook met elkaar afgesproken dat vandaag het volledige concept af moet zijn. We hebben toen als eerste met elkaar alles op een rijtje gezet en de ideeën gepakt die het beste bij het concept paste. Daarna hebben we gezamenlijk een samenvatting gemaakt van ons concept

