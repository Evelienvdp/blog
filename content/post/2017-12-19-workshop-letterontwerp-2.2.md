﻿---
title: Workshop letterontwerp 2.2
date: 2017-12-19
---

Vandaag heb ik het tweede deel van de workshop letterontwerp gevolgd. In deze workshop gingen verder over de theorieën van verschillende lettertype stijlen. Over de dezelfde diktes en de juiste verhoudingen die een lettertype moet hebben. 

Om dieper te gaan in het onderwerp moest je met verschillende materialen een woord schrijven/maken, aan een tafel. Aan tafel 1, heb ik met duktape het woord kerst geschreven. Hiermee heb ik een paar scheuren gemaakt in de tape, dit omdat ik het niet leuk vond om alleen maar rechte strepen te doen. 
Verder bij tafel 2 moest je met inkt woorden gaan schrijven. Hiervoor kon je een kwats, spons, rietjes of een wattenstaafje gebruiken om ermee te werken. Dit vond ik zelf heel erg leuk, vooral om je zo goed kon zien dat je met verschillende materialen andere dingen kon maken. 
Als laatste heb ik met ruitenpapier ook weer woorden geschreven. Dit vond ik in het begin best lastig, ik wist namelijk niet precies hoe ik met het blad mooie letter kon maken. Ik vond ze namelijk allemaal heel lelijk gemaakt door mij. Uiteindelijk heb ik toch wel een lettertype gemaakt waarmee ik tevreden was. 

> Ik vond deze workshop heel leuk en interessant omdat je door het gebruik van verschillende materialen andere vormen krijgt die weer een ander beeld geven van een lettertype. Zo kwam ik achter nieuwe letter vormen die ik zelf nog niet eerder had geprobeerd. Ik ga zeker van deze workshop gebruik maken als ik producten ga uitewerken tijdens het project.